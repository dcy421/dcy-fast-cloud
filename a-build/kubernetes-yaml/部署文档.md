# kubernetes部署文档


## 1创建命名空间
```shell script
kubectl apply -f dcy-fast-cloud-ns.yaml
```
> 如果使用自己的harbor仓库执行命令：
```shell script
kubectl create secret docker-registry harborkey \
--docker-server=http://harbor地址 \
--docker-username=harbor用户名 \
--docker-password=harbor密码  \
-n dcy-dev
```

## 2创建配置文件
```shell script
kubectl apply -n dcy-dev -f dcy-fast-cloud-config-map.yaml
```

## 3skywalking-agent构建

[kubernetes 制作skywalking的agent镜像](https://blog.csdn.net/qq_33842795/article/details/115631685)  
[kubernetes 部署spring boot项目+skywalking链路追踪](https://blog.csdn.net/qq_33842795/article/details/115632096)

## 4部署服务

```shell script
# 后端服务
kubectl apply -n dcy-dev -f auth-center.yaml
kubectl apply -n dcy-dev -f file-center.yaml
kubectl apply -n dcy-dev -f system-center.yaml

# 网关
kubectl apply -n dcy-dev -f gateway-center.yaml

# 前端服务
kubectl apply -n dcy-dev -f dcy-fast-cloud-vue.yaml
```
> 注意镜像名称和skywalking的agent镜像地址