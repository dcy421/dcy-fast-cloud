/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80029
 Source Host           : localhost:3306
 Source Schema         : dcy-fast-system

 Target Server Type    : MySQL
 Target Server Version : 80029
 File Encoding         : 65001

 Date: 28/04/2023 10:34:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数id',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数键名',
  `config_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '系统内置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1377930337229471746', '测试配置', 'test_config_key', 'userinfo', '0');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门id',
  `parent_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '祖级列表',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '部门名称',
  `dept_sort` decimal(20, 0) NULL DEFAULT 0 COMMENT '显示顺序',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `dept_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '部门状态（0、正常；1、停用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('1', '0', '0', '总公司', 0, NULL, NULL, '0');
INSERT INTO `sys_dept` VALUES ('1371702174283571201', '3', '0,1,3', '财务部门', 5, NULL, NULL, '0');
INSERT INTO `sys_dept` VALUES ('1377935436483919873', '2', '0,1,2', '研发部门', 1, NULL, NULL, '0');
INSERT INTO `sys_dept` VALUES ('1377935480696078337', '2', '0,1,2', '测试部门', 3, NULL, NULL, '0');
INSERT INTO `sys_dept` VALUES ('1377935517522067457', '2', '0,1,2', '产品部门', 5, NULL, NULL, '0');
INSERT INTO `sys_dept` VALUES ('1377935562556309505', '2', '0,1,2', '运维部门', 7, NULL, NULL, '0');
INSERT INTO `sys_dept` VALUES ('1377935601739497474', '2', '0,1,2', '市场部门', 8, NULL, NULL, '0');
INSERT INTO `sys_dept` VALUES ('1377936163021258753', '3', '0,1,3', '市场部门', 3, NULL, NULL, '0');
INSERT INTO `sys_dept` VALUES ('2', '1', '0,1', '深圳总公司', 1, NULL, NULL, '0');
INSERT INTO `sys_dept` VALUES ('3', '1', '0,1', '长沙分公司', 2, '22323', '11111', '0');

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典id',
  `parent_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父级id',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `dict_sort` int NULL DEFAULT 0 COMMENT '字典排序',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `dict_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1372709863595810818', NULL, '男', '1', 'sex', 1, NULL, 'primary', '0');
INSERT INTO `sys_dict_data` VALUES ('1372709863595810819', NULL, '女', '2', 'sex', 2, NULL, 'success', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710732080979960', NULL, '正常', '0', 'dict_status', 1, NULL, 'success', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710732080979962', NULL, '正常', '0', 'role_status', 1, NULL, 'success', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710732080979966', NULL, '正常', '0', 'dept_status', 1, NULL, 'success', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710732080979967', NULL, '正常', '0', 'post_status', 1, NULL, 'success', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710732080979969', NULL, '正常', '0', 'user_status', 1, NULL, 'success', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710732080979980', NULL, '正常', '0', 'res_status', 1, NULL, 'success', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710783326986241', NULL, '禁用', '1', 'user_status', 2, NULL, 'danger', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710783326986242', NULL, '禁用', '1', 'dict_status', 2, NULL, 'danger', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710783326986243', NULL, '禁用', '1', 'role_status', 2, NULL, 'danger', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710783326986245', NULL, '禁用', '1', 'dept_status', 2, NULL, 'danger', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710783326986248', NULL, '禁用', '1', 'post_status', 2, NULL, 'danger', '0');
INSERT INTO `sys_dict_data` VALUES ('1372710783326986281', NULL, '禁用', '1', 'res_status', 2, NULL, 'danger', '0');
INSERT INTO `sys_dict_data` VALUES ('1372711039930310657', NULL, '管理员', '0', 'user_type', 1, NULL, NULL, '0');
INSERT INTO `sys_dict_data` VALUES ('1372711039930310673', NULL, '目录', '1', 'resource_type', 1, NULL, NULL, '0');
INSERT INTO `sys_dict_data` VALUES ('1372711076852768769', NULL, '普通用户', '1', 'user_type', 2, NULL, NULL, '0');
INSERT INTO `sys_dict_data` VALUES ('1372711076852768774', NULL, '菜单', '2', 'resource_type', 2, NULL, NULL, '0');
INSERT INTO `sys_dict_data` VALUES ('1376409914658590721', NULL, '全部数据权限', '1', 'data_scope', 1, NULL, 'primary', '0');
INSERT INTO `sys_dict_data` VALUES ('1376409951539105794', NULL, '自定数据权限', '2', 'data_scope', 2, NULL, 'success', '0');
INSERT INTO `sys_dict_data` VALUES ('1376409996724342786', NULL, '本部门数据权限', '3', 'data_scope', 3, NULL, 'danger', '0');
INSERT INTO `sys_dict_data` VALUES ('1376410032313012226', NULL, '本部门及以下数据权限', '4', 'data_scope', 4, NULL, 'error', '0');
INSERT INTO `sys_dict_data` VALUES ('1378139151967047682', NULL, '是', '0', 'config_type', 0, NULL, 'success', '0');
INSERT INTO `sys_dict_data` VALUES ('1378139197374582785', NULL, '否', '1', 'config_type', 1, NULL, 'danger', '0');
INSERT INTO `sys_dict_data` VALUES ('1455847279071858690', NULL, '按钮', '3', 'resource_type', 3, NULL, NULL, '0');

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `dict_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1372709749485576194', '性别', 'sex', '0');
INSERT INTO `sys_dict_type` VALUES ('1372710292954128385', '用户类型', 'user_type', '0');
INSERT INTO `sys_dict_type` VALUES ('1372710383374934017', '用户状态', 'user_status', '0');
INSERT INTO `sys_dict_type` VALUES ('1372710383374934018', '字典状态', 'dict_status', '0');
INSERT INTO `sys_dict_type` VALUES ('1372714386955558914', '角色状态', 'role_status', '0');
INSERT INTO `sys_dict_type` VALUES ('1372714386955558915', '部门状态', 'dept_status', '0');
INSERT INTO `sys_dict_type` VALUES ('1372714386955558916', '岗位状态', 'post_status', '0');
INSERT INTO `sys_dict_type` VALUES ('1372720409946746881', '模块类型', 'resource_type', '0');
INSERT INTO `sys_dict_type` VALUES ('1372742181504978945', '资源状态', 'res_status', '0');
INSERT INTO `sys_dict_type` VALUES ('1376409832819331073', '数据范围', 'data_scope', '0');
INSERT INTO `sys_dict_type` VALUES ('1378139043670118402', '参数配置系统内置选项', 'config_type', '0');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '岗位名称',
  `post_sort` decimal(10, 2) NULL DEFAULT NULL COMMENT '显示顺序',
  `post_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '岗位状态（0、正常；1、停用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES ('1372012058585690114', 'ceo', '董事长', 1.00, '0');
INSERT INTO `sys_post` VALUES ('1372053095446319106', 'xxx', '研发经理', 2.00, '0');
INSERT INTO `sys_post` VALUES ('1372053161728905217', 'test', '测试经理', 3.00, '0');

-- ----------------------------
-- Table structure for sys_resource
-- ----------------------------
DROP TABLE IF EXISTS `sys_resource`;
CREATE TABLE `sys_resource`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键id',
  `parent_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父级id',
  `parent_ids` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '父级ids',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题（目录名称、菜单名称、按钮名称）',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型（1、目录；2、菜单；3、按钮）',
  `permission` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识（菜单和按钮）',
  `route_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由地址（目录和菜单）',
  `component_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单组件名称',
  `component_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单组件地址',
  `res_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '状态（0、正常；1、禁用）',
  `res_sort` decimal(10, 2) NULL DEFAULT NULL COMMENT '排序',
  `menu_ext_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '外链菜单（1：是；2：否）',
  `menu_cache_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单缓存（1：是；2：否）',
  `menu_hidden_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单和目录可见（1：是；2：否）',
  `menu_icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '资源表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_resource
-- ----------------------------
INSERT INTO `sys_resource` VALUES ('1173787141281456130', '1455703600814428162', '0,1455496504403607553,1455703600814428162', '角色添加', '3', 'role:save', NULL, NULL, NULL, '0', 1002.10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1173787273578192898', '1455703600814428162', '0,1455496504403607553,1455703600814428162', '角色修改', '3', 'role:update', NULL, NULL, NULL, '0', 1002.15, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1173787371326447617', '1455703600814428162', '0,1455496504403607553,1455703600814428162', '角色删除', '3', 'role:delete', NULL, NULL, NULL, '0', 1002.20, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1173787686142517250', '1455703600814428162', '0,1455496504403607553,1455703600814428162', '授权', '3', 'role:auth:resource', NULL, NULL, NULL, '0', 1002.25, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1173793141136859137', '1455496906708664322', '0,1455496504403607553,1455496906708664322', '资源添加', '3', 'resource:add', NULL, NULL, NULL, '0', 1003.10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1173793218580488194', '1455496906708664322', '0,1455496504403607553,1455496906708664322', '资源修改', '3', 'resource:update', NULL, NULL, NULL, '0', 1003.14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1173793287136387073', '1455496906708664322', '0,1455496504403607553,1455496906708664322', '资源删除', '3', 'resource:delete', NULL, NULL, NULL, '0', 1003.18, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1208938659106000810', '1455777270018715649', '0,1455496504403607553,1455777270018715649', '字典修改', '3', 'dict:update', NULL, NULL, NULL, '0', 1006.04, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1208938659106000811', '1455777270018715649', '0,1455496504403607553,1455777270018715649', '字典删除', '3', 'dict:delete', NULL, NULL, NULL, '0', 1006.09, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1208938659106000845', '1455777270018715649', '0,1455496504403607553,1455777270018715649', '字典列表', '3', 'dict:data:list', NULL, NULL, NULL, '0', 1006.14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1208938659106000898', '1455777270018715649', '0,1455496504403607553,1455777270018715649', '字典添加', '3', 'dict:save', NULL, NULL, NULL, '0', 1006.14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377928808594419714', '1455776367157030914', '0,1455496504403607553,1455776367157030914', '部门添加', '3', 'dept:save', NULL, NULL, NULL, '0', 1004.02, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377928808594419715', '1455776367157030914', '0,1455496504403607553,1455776367157030914', '部门修改', '3', 'dept:update', NULL, NULL, NULL, '0', 1004.04, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377928808594419716', '1455776367157030914', '0,1455496504403607553,1455776367157030914', '部门删除', '3', 'dept:delete', NULL, NULL, NULL, '0', 1004.12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377929323734642691', '1455776928103247874', '0,1455496504403607553,1455776928103247874', '岗位添加', '3', 'post:save', NULL, NULL, NULL, '0', 1005.04, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377929323734642692', '1455776928103247874', '0,1455496504403607553,1455776928103247874', '岗位修改', '3', 'post:update', NULL, NULL, NULL, '0', 1005.08, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377929323734642693', '1455776928103247874', '0,1455496504403607553,1455776928103247874', '岗位删除', '3', 'post:delete', NULL, NULL, NULL, '0', 1005.14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377930633724821506', '1455777407700938753', '0,1455496504403607553,1455777407700938753', '配置添加', '3', 'config:save', NULL, NULL, NULL, '0', 1007.02, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377930633724821507', '1455777407700938753', '0,1455496504403607553,1455777407700938753', '配置修改', '3', 'config:update', NULL, NULL, NULL, '0', 1007.07, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377930633724821508', '1455777407700938753', '0,1455496504403607553,1455777407700938753', '配置删除', '3', 'config:delete', NULL, NULL, NULL, '0', 1007.14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1377930633724821509', '1455777407700938753', '0,1455496504403607553,1455777407700938753', '配置删除（批量）', '3', 'config:batch:delete', NULL, NULL, NULL, '0', 1007.16, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `sys_resource` VALUES ('1455496504403607553', '0', '0', '系统管理', '1', NULL, '/admin', 'admin', 'Layout', '0', 1000.00, '2', '2', '1', 'system', '2021-11-02 19:26:23');
INSERT INTO `sys_resource` VALUES ('1455496906708664321', '1455496504403607553', '0,1455496504403607553', '用户管理', '2', 'user:list', 'user-manage', 'user-manage', 'admin/user/user-manage', '0', 1001.00, '2', '2', '1', 'user', '2021-11-02 19:27:59');
INSERT INTO `sys_resource` VALUES ('1455496906708664322', '1455496504403607553', '0,1455496504403607553', '资源管理', '2', 'resource:list', 'resource-manage', 'resource-manage', 'admin/resource/resource-manage', '0', 1003.00, '2', '2', '1', 'people', '2021-11-02 19:27:59');
INSERT INTO `sys_resource` VALUES ('1455703600814428162', '1455496504403607553', '0,1455496504403607553', '角色管理', '2', 'role:list', 'role-manage', 'role-manage', 'admin/role/role-manage', '0', 1002.00, '2', '2', '1', 'role', '2021-11-03 09:09:19');
INSERT INTO `sys_resource` VALUES ('1455710855135870977', '0', '0', '嵌套菜单', '1', NULL, '/nested', 'nested', 'Layout', '0', 1200.00, '2', '2', '1', 'nested', '2021-11-03 09:38:08');
INSERT INTO `sys_resource` VALUES ('1455711672903516161', '1455710855135870977', '0,1455710855135870977', '菜单1', '1', NULL, '/menu1', 'menu1', 'ParentView', '0', 500.00, '2', '2', '1', NULL, '2021-11-03 09:41:23');
INSERT INTO `sys_resource` VALUES ('1455711998473781250', '1455711672903516161', '0,1455710855135870977,1455711672903516161', '菜单1-1', '2', NULL, 'menu1-1', 'menu1-1', 'nested/menu1/menu1-1/index', '0', 500.00, '2', '2', '1', NULL, '2021-11-03 09:42:41');
INSERT INTO `sys_resource` VALUES ('1455712265919381505', '1455711672903516161', '0,1455710855135870977,1455711672903516161', '菜单1-2', '1', NULL, 'menu1-2', 'menu1-2', 'ParentView', '0', 502.00, '2', '2', '1', NULL, '2021-11-03 09:43:45');
INSERT INTO `sys_resource` VALUES ('1455712369480941569', '1455712265919381505', '0,1455710855135870977,1455711672903516161,1455712265919381505', '菜单1-2-1', '2', NULL, 'menu1-2-1', 'menu1-2-1', 'nested/menu1/menu1-2/menu1-2-1/index', '0', 500.00, '2', '2', '1', NULL, '2021-11-03 09:44:09');
INSERT INTO `sys_resource` VALUES ('1455712458534404098', '1455712265919381505', '0,1455710855135870977,1455711672903516161,1455712265919381505', '菜单1-2-2', '2', NULL, 'menu1-2-2', 'menu1-2-2', 'nested/menu1/menu1-2/menu1-2-2/index', '0', 502.00, '2', '2', '1', NULL, '2021-11-03 09:44:31');
INSERT INTO `sys_resource` VALUES ('1455712586129326081', '1455711672903516161', '0,1455710855135870977,1455711672903516161', '菜单1-3', '2', NULL, 'menu1-3', 'menu1-3', 'nested/menu1/menu1-3/index', '0', 504.00, '2', '2', '1', NULL, '2021-11-03 09:45:01');
INSERT INTO `sys_resource` VALUES ('1455712684905185281', '1455710855135870977', '0,1455710855135870977', '菜单2', '2', NULL, '/menu2', 'menu2', 'nested/menu2/index', '0', 502.00, '2', '2', '1', NULL, '2021-11-03 09:45:25');
INSERT INTO `sys_resource` VALUES ('1455774928233910274', '1455496504403607553', '0,1455496504403607553', '接口文档', '2', NULL, 'http://localhost:8999/doc.html', NULL, NULL, '0', 1011.00, '1', '2', '1', 'nested', '2021-11-03 13:52:45');
INSERT INTO `sys_resource` VALUES ('1455776367157030914', '1455496504403607553', '0,1455496504403607553', '部门管理', '2', 'dept:list', 'dept-manage', 'dept-manage', 'admin/dept/dept-manage', '0', 1004.00, '2', '2', '1', 'nested', '2021-11-03 13:58:28');
INSERT INTO `sys_resource` VALUES ('1455776928103247874', '1455496504403607553', '0,1455496504403607553', '岗位管理', '2', 'post:list', 'post-manage', 'post-manage', 'admin/post/post-manage', '0', 1005.00, '2', '2', '1', 'nested', '2021-11-03 14:00:41');
INSERT INTO `sys_resource` VALUES ('1455777270018715649', '1455496504403607553', '0,1455496504403607553', '字典管理', '2', 'dict:list', 'dict-manage', 'dict-manage', 'admin/dict/dict-type-manage', '0', 1006.00, '2', '2', '1', 'nested', '2021-11-03 14:02:03');
INSERT INTO `sys_resource` VALUES ('1455777407700938753', '1455496504403607553', '0,1455496504403607553', '配置管理', '2', 'config:list', 'config-manage', 'config-manage', 'admin/config/config-manage', '0', 1007.00, '2', '2', '1', 'nested', '2021-11-03 14:02:36');
INSERT INTO `sys_resource` VALUES ('1455777952633303042', '1455496504403607553', '0,1455496504403607553', '数据监控', '2', NULL, 'http://localhost:8999/druid/login.html', NULL, NULL, '0', 1010.00, '1', '2', '1', 'nested', '2021-11-03 14:04:46');
INSERT INTO `sys_resource` VALUES ('1455801448314703873', '1455496906708664321', '0,1455496504403607553,1455496906708664321', '用户添加', '3', 'user:save', NULL, NULL, NULL, '0', 1001.20, NULL, NULL, NULL, NULL, '2021-11-03 15:38:07');
INSERT INTO `sys_resource` VALUES ('1455836405246464001', '1455496906708664321', '0,1455496504403607553,1455496906708664321', '用户修改', '3', 'user:update', NULL, NULL, NULL, '0', 1001.31, NULL, NULL, NULL, NULL, '2021-11-03 17:57:02');
INSERT INTO `sys_resource` VALUES ('1455836405246464002', '1455496906708664321', '0,1455496504403607553,1455496906708664321', '用户删除', '3', 'user:delete', NULL, NULL, NULL, '0', 1001.35, NULL, NULL, NULL, NULL, '2021-11-03 17:57:02');
INSERT INTO `sys_resource` VALUES ('1455836405246464004', '1455496906708664321', '0,1455496504403607553,1455496906708664321', '重置密码', '3', 'user:reset:pass', NULL, NULL, NULL, '0', 1001.45, NULL, NULL, NULL, NULL, '2021-11-03 17:57:02');
INSERT INTO `sys_resource` VALUES ('1455836405246464005', '1455496906708664321', '0,1455496504403607553,1455496906708664321', '授权角色', '3', 'user:auth:role', NULL, NULL, NULL, '0', 1001.50, NULL, NULL, NULL, NULL, '2021-11-03 17:57:02');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色id',
  `role_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_key` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色权限字符串',
  `role_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色状态（0、正常；1、禁用）',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '数据范围（1、全部数据权限；2、自定数据权限；3、本部门数据权限；4、本部门及以下数据权限）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1171709223680184321', '管理员', 'ROLE_ADMIN', '0', '1');
INSERT INTO `sys_role` VALUES ('1171953892250918913', '开发组长', 'ROLE_DEVELOP', '0', '4');
INSERT INTO `sys_role` VALUES ('1171953965877731330', '测试组长', 'ROLE_TEST', '0', '4');
INSERT INTO `sys_role` VALUES ('1171954063797952514', '项目经理', 'ROLE_MANAGE', '1', '1');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色id',
  `dept_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门id',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_res
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_res`;
CREATE TABLE `sys_role_res`  (
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色id',
  `res_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '资源id',
  PRIMARY KEY (`role_id`, `res_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和资源关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_res
-- ----------------------------
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1173787141281456130');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1173787273578192898');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1173787371326447617');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1173787686142517250');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1173793141136859137');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1173793218580488194');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1173793287136387073');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1208938659106000810');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1208938659106000811');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1208938659106000845');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1208938659106000898');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377928808594419714');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377928808594419715');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377928808594419716');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377929323734642691');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377929323734642692');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377929323734642693');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377930633724821506');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377930633724821507');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377930633724821508');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1377930633724821509');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455496504403607553');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455496906708664321');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455496906708664322');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455703600814428162');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455710855135870977');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455711672903516161');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455711998473781250');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455712265919381505');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455712369480941569');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455712458534404098');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455712586129326081');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455712684905185281');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455774928233910274');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455776367157030914');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455776928103247874');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455777270018715649');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455777407700938753');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455777952633303042');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455801448314703873');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455836405246464001');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455836405246464002');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455836405246464004');
INSERT INTO `sys_role_res` VALUES ('1171709223680184321', '1455836405246464005');
INSERT INTO `sys_role_res` VALUES ('1171953892250918913', '1455496504403607553');
INSERT INTO `sys_role_res` VALUES ('1171953892250918913', '1455496906708664321');
INSERT INTO `sys_role_res` VALUES ('1171953892250918913', '1455801448314703873');
INSERT INTO `sys_role_res` VALUES ('1171953892250918913', '1455836405246464001');
INSERT INTO `sys_role_res` VALUES ('1171953892250918913', '1455836405246464002');
INSERT INTO `sys_role_res` VALUES ('1171953892250918913', '1455836405246464004');
INSERT INTO `sys_role_res` VALUES ('1171953892250918913', '1455836405246464005');

-- ----------------------------
-- Table structure for sys_user_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_info`;
CREATE TABLE `sys_user_info`  (
  `id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `dept_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门id',
  `username` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nick_name` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户昵称',
  `user_type` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '用户类型（0、管理员；1、普通用户）',
  `email` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户邮箱',
  `phone_number` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '性别（0、男；1、女）',
  `avatar_path` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '头像',
  `user_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '帐号状态（0、正常；1、禁用）',
  `create_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `update_date` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '删除标识',
  `remark` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_info
-- ----------------------------
INSERT INTO `sys_user_info` VALUES ('1170896100656156674', '1', 'admin', '$2a$10$I3nz8bGJfgpKcZbUSJnc8.PQxAYXdJP6r.eLHzdsfBLsCOx8JSB76', '管理员', '0', '13223423@qq.com', '15988888885', '1', NULL, '0', NULL, '2021-04-03 09:19:01', '1170896100656156674', '2021-04-03 09:25:21', '0', '管理员');
INSERT INTO `sys_user_info` VALUES ('1171948965562806274', '2', '1234567', '$2a$10$O2YavjwTheFjryKJSrZGv.aixXnZ1K6GgtCAxEMg5KXSC6gvCDKSy', 'dd', '1', 'dsfa', '112312', '1', NULL, '0', '1170896100656156674', NULL, '1170896100656156674', NULL, '0', 'dd');
INSERT INTO `sys_user_info` VALUES ('1298494547157360642', '3', '121212121', '$2a$10$XryvgFQCGnLn88wV13lm.ezWYcxlPd8VsRWdYa98lqCLwLCya4Aaa', '千千万万1', '1', 'ddasfsda@qq', '154123123213', '1', NULL, '0', NULL, '2020-08-25 13:36:40', '1170896100656156674', '2021-03-17 13:49:10', '0', 'ddd');
INSERT INTO `sys_user_info` VALUES ('1339368705646702593', '3', '12345676', '$2a$10$U1GwFyVo1.xXknTYIbU72OlHox2S8OCkzSd0pAmIZV.MYLtuMhzwe', '1116', '1', '11116', '123123412346', '2', NULL, '1', NULL, NULL, '1170896100656156674', '2021-03-17 13:49:05', '0', NULL);
INSERT INTO `sys_user_info` VALUES ('1457667422181769218', '1377935436483919873', '121212121', '$2a$10$Jg3Ir0FyvVyFyVJrfnCpuO//OFEIF3PzA2HxR72Yy1otW9BbeOsp.', '121212', '1', NULL, '111', '1', NULL, '0', '1170896100656156674', '2021-11-08 19:12:50', NULL, NULL, '0', NULL);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `post_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位id',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES ('1298494547157360642', '1372012058585690114');
INSERT INTO `sys_user_post` VALUES ('1298494547157360642', '1372053095446319106');
INSERT INTO `sys_user_post` VALUES ('1298494547157360642', '1372053161728905217');
INSERT INTO `sys_user_post` VALUES ('1298494547157360642', '1372053279836311554');
INSERT INTO `sys_user_post` VALUES ('1339368705646702593', '1372012058585690114');
INSERT INTO `sys_user_post` VALUES ('1457667422181769218', '1372053095446319106');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户id',
  `role_id` varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1170896100656156674', '1171709223680184321');
INSERT INTO `sys_user_role` VALUES ('1171948965562806274', '1171953892250918913');

SET FOREIGN_KEY_CHECKS = 1;
