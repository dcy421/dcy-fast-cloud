# mapstruct文档

官网地址：[https://mapstruct.org/](https://mapstruct.org/)

# 使用帮助

## maven配置
```xml
<dependencies>
    <!-- 对象转换使用 -->
    <dependency>
        <groupId>org.mapstruct</groupId>
        <artifactId>mapstruct</artifactId>
        <version>${mapstruct.version}</version>
    </dependency>
</dependencies>

<build>
    <plugins>
        <!-- lombok 整合 mapstruct 必须添加此插件，否则build没有对应spring组件-->
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.8.1</version>
            <configuration>
                <source>${java.version}</source>
                <target>${java.version}</target>
                <annotationProcessorPaths>
                    <path>
                        <groupId>org.projectlombok</groupId>
                        <artifactId>lombok</artifactId>
                        <version>${lombok.version}</version>
                    </path>
                    <path>
                        <groupId>org.mapstruct</groupId>
                        <artifactId>mapstruct-processor</artifactId>
                        <version>${mapstruct.version}</version>
                    </path>
                </annotationProcessorPaths>
            </configuration>
        </plugin>
    </plugins>
</build>
```

## 定义Mapper接口

```java
@Mapper(componentModel = "spring")
public interface MUserInfoMapper {

    
//    @Mappings({
//            @Mapping(source = "username",target = "username"),  //相同可以不写
//            @Mapping(source = "a字段",target = "b字段")  //不同的时候
//    })
//    UserInfo loginInputDTOToUserInfo(LoginInputDTO loginInVO);

    UserInfo toUserInfo(UserInfoCreateInputDTO userInfoCreateInVO);

    UserInfo toUserInfo(UserInfoUpdateInputDTO userInfoUpdateInVO);

    UserInfo toUserInfo(UserInfoUpdateInfoInputDTO userInfoUpdateInfoInVO);

    UserInfo toUserInfo(UserInfoResetPassInputDTO userInfoResetPassInVO);


    UserInfo toUserInfo(UserInfoSearchInputDTO userInfoSearchInVO);

    UserInfoListOutputDTO toList(UserInfo userInfo);

    List<UserInfoListOutputDTO> toList(List<UserInfo> userInfos);


    LoginOutputDTO toLoginOutputDTO(UserInfo userInfo);
}
```

> 1. @Mapper(componentModel = "spring") 表示把此接口变成spring组件
> 2. @Mappings 可以映射不同字段名称
> 3. 其他文档请看官网介绍，用法及其简单


## 使用方式

> 和其他ioc组件用法一样，`@Autowired` 注入即可
```java
@Autowired
private MUserInfoMapper mUserInfoMapper;


```

# 常见问题：
1. 写好了Mapper文件，已经build完成了，但是再次添加字段，无法转换字段。  
在此模块 执行 maven clean，清空target文件，从新生成即可。