#!/bin/bash

# 模块名称
MODULE=$1
# 脚本路径
SCRIPT_PATH=$2
# 环境
ENV=$3
# 项目类型
TYPE=$4
# 路径前缀
PATH_PREFIX=$5
# 构建流水号
BUILD_NUMBER=$6


# harbor项目名称
project='dcy-fast-cloud-dev'
harborHost='harbor地址'
harborUserName='用户名'
harborPassword='密码'
if [ ${ENV} == 'test' ]; then
  project='dcy-fast-cloud-test'
elif [ ${ENV} == 'prod' ]; then
  # 暂时空着
  project='dcy-fast-cloud-dev'
  harborHost='harbor地址'
  harborUserName='用户名'
  harborPassword='密码'
fi

# 拼接镜像名称
IMAGE_NAME=${harborHost}/${project}/${MODULE}:${TIME}_${BUILD_NUMBER}

# 进入模块目录
cd ${PATH_PREFIX}${MODULE}

# 打包
if [ ${TYPE} == 'java' ]; then
  docker build -t ${IMAGE_NAME} -f src/main/docker/Dockerfile .
else
  docker build -t ${IMAGE_NAME} -f docker/Dockerfile .
fi

# 登录
docker login ${harborHost} -u ${harborUserName} -p ${harborPassword}

# pull镜像
docker push ${IMAGE_NAME}

# 退出
docker logout ${harborHost}

# 创建临时文件夹
mkdir -p ${SCRIPT_PATH}/${ENV}

# 写临时文件
echo "${IMAGE_NAME}" >${SCRIPT_PATH}/${ENV}/${MODULE}:IMAGE_NAME
