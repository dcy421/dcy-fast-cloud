package com.dcy.db.base.dao;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.dcy.common.model.PageModel;
import com.dcy.db.base.mapper.DcyBaseMapper;

import java.util.List;

/**
 * @author dcy
 * @description 公共dao
 * @createTime 2022/11/8 14:41
 */
public class DcyBaseDao<M extends DcyBaseMapper<T>, T> extends ServiceImpl<M, T> {

    public static final String ORDER_ASC = "asc";
    public static final String ORDER_DESC = "desc";

    /**
     * 分页查询 根据分页参数查询
     *
     * @param pageModel    分页参数
     * @param queryWrapper 条件构造
     * @return
     */
    public IPage<T> pageList(PageModel pageModel, Wrapper<T> queryWrapper) {
        return page(getPagePlusInfo(pageModel), queryWrapper);
    }

    /**
     * 获取分页参数
     *
     * @param pageModel 分页入参对象
     * @return
     */
    protected IPage<T> getPagePlusInfo(PageModel pageModel) {
        Page<T> page = Page.of(pageModel.getCurrent(), pageModel.getSize());
        if (StrUtil.isNotBlank(pageModel.getOrder()) && StrUtil.isNotBlank(pageModel.getSort())) {
            final List<String> sortList = StrUtil.split(pageModel.getSort(), ',');
            final List<String> orderList = StrUtil.split(pageModel.getOrder(), ',');
            for (int i = 0; i < sortList.size(); i++) {
                String field = CollUtil.get(sortList, i);
                if (StrUtil.isBlank(field)) {
                    break;
                }
                field = StrUtil.toUnderlineCase(field);
                final String orderType = ObjectUtil.defaultIfBlank(CollUtil.get(orderList, i), ORDER_ASC);
                if (orderType.equals(ORDER_DESC)) {
                    page.addOrder(OrderItem.desc(field));
                } else {
                    page.addOrder(OrderItem.asc(field));
                }
            }
        }
        return page;
    }


}
