package com.dcy.db.base.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Author：dcy
 * @Description: 公共类删除标识枚举类
 * @Date: 2021/9/2 15:28
 */
@AllArgsConstructor
@Getter
public enum BaseModelDelFlagEnum {

    //删除标识（0：正常；1：已删除）
    NORMAL("0", "正常"),
    DELETED("1", "已删除"),
    ;

    private final String code;
    private final String name;


}
