package com.dcy.db.config;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.extension.MybatisMapWrapperFactory;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.dcy.db.injector.DcyDefaultSqlInjector;
import com.dcy.db.mybatis.handler.DcyDataPermissionHandler;
import com.dcy.db.mybatis.handler.DcyDataPermissionInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author：dcy
 * @Description: mybatis-plus 配置类
 * @Date: 2019/9/6 10:43
 */
@Configuration
public class MybatisPlusConfig {

    /**
     * 分页
     *
     * @return
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        // 定义数据权限拦截器
        final DcyDataPermissionInterceptor dcyDataPermissionInterceptor = new DcyDataPermissionInterceptor();
        dcyDataPermissionInterceptor.setDataPermissionHandler(new DcyDataPermissionHandler());

        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        // 数据权限拦截器
        mybatisPlusInterceptor.addInnerInterceptor(dcyDataPermissionInterceptor);
        //分页拦截器
        mybatisPlusInterceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        // 攻击 SQL 阻断解析器,防止全表更新与删除
        mybatisPlusInterceptor.addInnerInterceptor(new BlockAttackInnerInterceptor());
        return mybatisPlusInterceptor;
    }

    /**
     * 扩展 mybatis-plus baseMapper  支持公共方法
     *
     * @return
     */
    @Bean
    public DcyDefaultSqlInjector dcyDefaultSqlInjector() {
        return new DcyDefaultSqlInjector();
    }

    /**
     * 下划线转骆峰
     *
     * @return
     */
    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        // 函数式编程
        return (configuration) -> {
            // 使用mybatis-plus 内置的
            configuration.setObjectWrapperFactory(new MybatisMapWrapperFactory());
        };
    }
}
