package com.dcy.db.base.controller;

import com.dcy.common.api.IErrorCode;
import com.dcy.common.model.R;

/**
 * @Author：dcy
 * @Description: 公共controller
 * @Date: 2019/9/6 13:19
 */
public class DcyBaseController {

    protected <T> R<T> success() {
        return R.success(null);
    }

    protected <T> R<T> success(T obj) {
        return R.success(obj);
    }

    protected <T> R<T> error(String msg) {
        return R.error(msg);
    }

    protected <T> R<T> error(IErrorCode errorCode, T obj) {
        return R.error(errorCode);
    }

}
