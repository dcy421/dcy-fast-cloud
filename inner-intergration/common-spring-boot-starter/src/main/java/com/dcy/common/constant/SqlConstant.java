package com.dcy.common.constant;

/**
 * @author dcy
 * @description sql常量
 * @createTime 2022/12/30 11:37
 */
public interface SqlConstant {

    /**
     * 默认getOne 拼接sql
     */
    String DEFAULT_ONE_LAST_SQL = "LIMIT 1";
    /**
     * 数据权限默认字段
     */
    String DEFAULT_DATA_SCOPE_COLUMN = "dept_id";
}
