package com.dcy.satoken.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @Author：dcy
 * @Description: 登录使用
 * @Date: 2020/12/17 7:56
 */
@ToString
@Getter
@Setter
@ApiModel
public class LoginAccount {

    @ApiModelProperty(value = "用户id")
    private String id;

    @ApiModelProperty(value = "部门id")
    private String deptId;

    @ApiModelProperty(value = "用户名")
    private String username;

    @ApiModelProperty(value = "用户昵称")
    private String nickName;

    @ApiModelProperty(value = "用户类型（0、管理员；1、普通用户）")
    private String userType;

    @ApiModelProperty(value = "用户邮箱")
    private String email;

    @ApiModelProperty(value = "手机号码")
    private String phoneNumber;

    @ApiModelProperty(value = "性别（0、男；1、女）")
    private String sex;

    @ApiModelProperty(value = "头像")
    private String avatarPath;

    @ApiModelProperty(value = "帐号状态（0、正常；1、禁用）")
    private String userStatus;

    @ApiModelProperty(value = "资源信息")
    private List<String> resources;

    @ApiModelProperty(value = "部门名称")
    private String deptName;

    @ApiModelProperty(value = "角色名称")
    private String roleName;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "是否拥有所有数据权限：true拥有；false未拥有")
    private Boolean allDataScopeFlag;

    @ApiModelProperty(value = "数据权限集合：如果allDataScopeFlag=true，那此集合为空；否则查询数据权限")
    private Set<String> dataScopes;
}
