package com.dcy.log.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2019/9/19 9:29
 */
@Data
@ConfigurationProperties(prefix = "dcy.log")
@RefreshScope
public class LogProperties {
    /**
     * 是否开启业务日志
     */
    private Boolean enable = Boolean.TRUE;
}
