package com.dcy.es.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/9 15:14
 */
@Getter
@Setter
@ToString
@ApiModel(value = "PageResultOutputDTO", description = "分页返回结果对象")
public class PageResultOutVO<T> {

    @ApiModelProperty(value = "总条数")
    private Long total;

    @ApiModelProperty(value = "总页数")
    private Integer pages;

    @ApiModelProperty(value = "列表数据")
    private List<T> records;
}
