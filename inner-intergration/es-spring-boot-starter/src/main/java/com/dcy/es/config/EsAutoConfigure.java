package com.dcy.es.config;

import com.dcy.es.service.BaseElasticService;
import org.elasticsearch.client.RestClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.elasticsearch.ElasticsearchRestClientProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/4/17 13:57
 */
@Configuration
@ConditionalOnClass(RestClient.class)
@EnableConfigurationProperties(ElasticsearchRestClientProperties.class)
@Import(BaseElasticService.class)
public class EsAutoConfigure {

}
