package com.dcy.monitor.biz;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/4/22 10:32
 */
@EnableAdminServer
@SpringBootApplication
public class MonitorCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MonitorCenterApplication.class, args);
    }
}
