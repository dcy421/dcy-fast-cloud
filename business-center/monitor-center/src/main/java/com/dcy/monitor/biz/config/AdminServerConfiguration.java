package com.dcy.monitor.biz.config;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import de.codecentric.boot.admin.server.cloud.discovery.InstanceDiscoveryListener;
import de.codecentric.boot.admin.server.cloud.discovery.ServiceInstanceConverter;
import de.codecentric.boot.admin.server.domain.entities.InstanceRepository;
import de.codecentric.boot.admin.server.services.InstanceRegistry;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @Author：dcy
 * @Description: admin-server 服务实例配置
 * @Date: 2021/4/22 14:24
 */
@Configuration
public class AdminServerConfiguration {

    @Primary
    @Bean
    @ConditionalOnMissingBean
    @ConfigurationProperties(prefix = "spring.boot.admin.discovery")
    public InstanceDiscoveryListener instanceDiscoveryListener(ServiceInstanceConverter serviceInstanceConverter,
                                                               DiscoveryClient discoveryClient, InstanceRegistry registry, InstanceRepository repository) {
        InstanceDiscoveryListener listener = new InstanceDiscoveryListener(discoveryClient, registry, repository);
        listener.setConverter(serviceInstanceConverter);
        // 排除service匹配模式
        listener.setIgnoredServices(ImmutableSet.of("consumers:*", "providers:*"));
        // 注册服务的必须包含的元信息配置
        listener.setInstancesMetadata(ImmutableMap.of("preserved.register.source", "SPRING_CLOUD"));
        return listener;
    }
}
