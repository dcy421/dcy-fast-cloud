package com.dcy.log.biz.controller;

import com.dcy.common.model.R;
import com.dcy.es.dto.PageResultOutVO;
import com.dcy.log.biz.vo.in.SearchInVO;
import com.dcy.log.biz.vo.out.ConsoleLog;
import com.dcy.log.biz.service.LogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/9 15:34
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/log")
@Api(value = "LogController", tags = {"日志操作接口"})
public class LogController {

    private final LogService logService;

    @ApiOperation(value = "获取业务日志分页查询")
    @GetMapping("/getBusinessLogPageList")
    protected R<PageResultOutVO<ConsoleLog>> getBusinessLogPageList(SearchInVO searchInVO) {
        return R.success(logService.getBusinessLogPageList(searchInVO));
    }

    @ApiOperation(value = "获取登录登出日志分页查询")
    @GetMapping("/getLoginLogPageList")
    protected R<PageResultOutVO<ConsoleLog>> getLoginLogPageList(SearchInVO searchInVO) {
        return R.success(logService.getLoginLogPageList(searchInVO));
    }
}
