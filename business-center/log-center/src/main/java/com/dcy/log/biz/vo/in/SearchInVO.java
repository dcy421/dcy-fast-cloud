package com.dcy.log.biz.vo.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author：dcy
 * @Description: 查询参数
 * @Date: 2020/10/9 15:12
 */
@Getter
@Setter
@ToString
@ApiModel(value = "SearchInputDTO", description = "查询输入参数")
public class SearchInVO {

    @ApiModelProperty(value = "当前页，默认 1", example = "1")
    private Integer current;

    @ApiModelProperty(value = "获取每页显示条数，默认 30", example = "30")
    private Integer size;

    @ApiModelProperty(value = "查询内容")
    private String search;
}
