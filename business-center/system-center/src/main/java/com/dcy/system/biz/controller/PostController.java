package com.dcy.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.dcy.common.annotation.Log;
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import com.dcy.system.biz.constant.PermissionConstant;
import com.dcy.system.biz.service.PostService;
import com.dcy.system.biz.vo.in.PostCreateInVO;
import com.dcy.system.biz.vo.in.PostSearchInVO;
import com.dcy.system.biz.vo.in.PostUpdateInVO;
import com.dcy.system.biz.vo.out.PostListOutVO;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 岗位表 前端控制器
 * </p>
 *
 * @author dcy
 * @since 2021-03-16
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/post")
@ApiSupport(order = 30)
@Api(value = "PostController", tags = {"岗位管理接口"})
public class PostController extends DcyBaseController {

    private final PostService postService;

    @Log
    @SaCheckPermission(value = PermissionConstant.POST_LIST, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "岗位表分页查询")
    @GetMapping("/page")
    public R<PageResult<PostListOutVO>> pageList(PostSearchInVO postSearchInVO, PageModel pageModel) {
        return success(postService.pageListByEntity(postSearchInVO, pageModel));
    }

    @Log
    @ApiOperation(value = "获取所有数据")
    @GetMapping("/all")
    public R<List<PostListOutVO>> listAll() {
        return success(postService.listAll());
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.POST_SAVE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody PostCreateInVO postCreateInVO) {
        return success(postService.save(postCreateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.POST_UPDATE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "修改")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody PostUpdateInVO postUpdateInVO) {
        return success(postService.update(postUpdateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.POST_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam String id) {
        return success(postService.delete(id));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.POST_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "根据list删除")
    @PostMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return success(postService.deleteBatch(idList));
    }

}
