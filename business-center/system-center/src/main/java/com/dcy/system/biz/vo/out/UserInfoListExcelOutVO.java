package com.dcy.system.biz.vo.out;

import cn.hutool.core.date.DatePattern;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import lombok.Data;

import java.util.Date;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 9:18
 */
@Data
public class UserInfoListExcelOutVO {

    @ExcelProperty(value = "用户名")
    @ColumnWidth(20)
    private String username;

    @ExcelProperty(value = "用户昵称")
    @ColumnWidth(20)
    private String nickName;

    @ExcelProperty(value = "用户邮箱")
    @ColumnWidth(20)
    private String email;

    @ExcelProperty(value = "手机号码")
    @ColumnWidth(20)
    private String phoneNumber;

    @ExcelProperty(value = "性别")
    @ColumnWidth(10)
    private String sexName;

    @ExcelProperty(value = "帐号状态")
    @ColumnWidth(15)
    private String userStatusName;

    @DateTimeFormat(DatePattern.NORM_DATETIME_MINUTE_PATTERN)
    @ExcelProperty(value = "创建时间")
    @ColumnWidth(20)
    private Date createDate;
}
