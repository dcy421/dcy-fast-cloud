package com.dcy.system.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.system.biz.model.Dept;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2021-03-16
 */
public interface DeptMapper extends DcyBaseMapper<Dept> {

}
