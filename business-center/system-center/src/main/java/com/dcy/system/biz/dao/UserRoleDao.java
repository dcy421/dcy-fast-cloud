package com.dcy.system.biz.dao;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dcy.db.base.dao.DcyBaseDao;
import com.dcy.system.biz.model.UserRole;
import com.dcy.system.biz.mapper.UserRoleMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


/**
 * <p>
 * 用户角色关联表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2022-11-08
 */
@Service
public class UserRoleDao extends DcyBaseDao<UserRoleMapper, UserRole> {

    /**
     * 根据用户id 查询
     *
     * @param userId
     * @return
     */
    public List<String> getRoleIdListByUserId(String userId) {
        return list(Wrappers.<UserRole>lambdaQuery().eq(UserRole::getUserId, userId))
                .stream().map(UserRole::getRoleId).collect(Collectors.toList());
    }

}
