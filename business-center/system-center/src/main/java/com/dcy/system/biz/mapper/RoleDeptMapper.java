package com.dcy.system.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.system.biz.model.RoleDept;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2021-03-29
 */
public interface RoleDeptMapper extends DcyBaseMapper<RoleDept> {

}
