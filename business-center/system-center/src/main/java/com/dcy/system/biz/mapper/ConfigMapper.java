package com.dcy.system.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.system.biz.model.Config;

/**
 * <p>
 * 参数配置表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
public interface ConfigMapper extends DcyBaseMapper<Config> {

}
