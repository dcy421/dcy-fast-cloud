package com.dcy.system.biz.convert;

import com.dcy.system.biz.model.DictData;
import com.dcy.system.biz.vo.in.DictDataCreateInVO;
import com.dcy.system.biz.vo.in.DictDataSearchInVO;
import com.dcy.system.biz.vo.in.DictDataUpdateInVO;
import com.dcy.system.biz.vo.out.DictDataListOutVO;
import com.dcy.system.biz.vo.out.DictDataSelOutVO;
import com.dcy.system.biz.vo.out.DictDataTreeSelOutVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 9:28
 */
@Mapper
public interface DictDataConvert {

    DictDataConvert INSTANCE = Mappers.getMapper(DictDataConvert.class);

    DictData toDictData(DictDataSearchInVO dictDataSearchInVO);

    DictData toDictData(DictDataCreateInVO dictDataCreateInVO);

    DictData toDictData(DictDataUpdateInVO dictDataUpdateInVO);

    DictDataListOutVO toOut(DictData dictData);

    DictDataSelOutVO toDictDataSelOut(DictData dictData);

    List<DictDataSelOutVO> toDictDataSelOutList(List<DictData> dictDatas);

    DictDataTreeSelOutVO toDictDataTreeSelOut(DictData dictData);

    List<DictDataTreeSelOutVO> toDictDataTreeSelOutList(List<DictData> dictDatas);
}
