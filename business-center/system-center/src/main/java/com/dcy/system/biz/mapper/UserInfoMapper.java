package com.dcy.system.biz.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.db.mybatis.annotation.DataScopeColumn;
import com.dcy.system.biz.model.UserInfo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2020-08-19
 */
public interface UserInfoMapper extends DcyBaseMapper<UserInfo> {

    /**
     * 分页查询
     *
     * @param page    分页对象
     * @param wrapper 条件参数
     * @return
     */
    @DataScopeColumn
    IPage<UserInfo> selectPageList(IPage<UserInfo> page, @Param(Constants.WRAPPER) Wrapper<UserInfo> wrapper);
}
