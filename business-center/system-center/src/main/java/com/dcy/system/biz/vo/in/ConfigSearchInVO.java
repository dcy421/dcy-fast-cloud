package com.dcy.system.biz.vo.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 15:07
 */
@Data
@ApiModel
public class ConfigSearchInVO {

    @ApiModelProperty(value = "参数名称")
    private String configName;
}
