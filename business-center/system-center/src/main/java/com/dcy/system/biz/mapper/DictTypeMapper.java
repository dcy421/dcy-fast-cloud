package com.dcy.system.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.system.biz.model.DictType;

/**
 * <p>
 * 字典类型表 Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2021-03-17
 */
public interface DictTypeMapper extends DcyBaseMapper<DictType> {

}
