package com.dcy.system.biz.convert;

import com.dcy.system.api.dto.out.DeptListOutDTO;
import com.dcy.system.biz.model.Dept;
import com.dcy.system.biz.vo.in.DeptCreateInVO;
import com.dcy.system.biz.vo.in.DeptSearchInVO;
import com.dcy.system.biz.vo.in.DeptUpdateInVO;
import com.dcy.system.biz.vo.out.DeptListOutVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 9:28
 */
@Mapper
public interface DeptConvert {

    DeptConvert INSTANCE = Mappers.getMapper(DeptConvert.class);

    Dept toDept(DeptSearchInVO deptSearchInVO);

    Dept toDept(DeptCreateInVO deptCreateInVO);

    Dept toDept(DeptUpdateInVO deptUpdateInVO);

    DeptListOutVO toOut(Dept dept);
    DeptListOutDTO toOutDto(Dept dept);

    List<DeptListOutVO> toOutList(List<Dept> depts);


}
