package com.dcy.system.biz.service;

import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.db.base.service.DcyBaseService;
import com.dcy.system.biz.convert.ConfigConvert;
import com.dcy.system.biz.dao.ConfigDao;
import com.dcy.system.biz.vo.in.ConfigCreateInVO;
import com.dcy.system.biz.vo.in.ConfigSearchInVO;
import com.dcy.system.biz.vo.in.ConfigUpdateInVO;
import com.dcy.system.biz.vo.out.ConfigListOutVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
@RequiredArgsConstructor
@Service
public class ConfigService extends DcyBaseService {

    private final ConfigDao configDao;
    private final ConfigConvert configConvert = ConfigConvert.INSTANCE;

    /**
     * 获取表格数据
     *
     * @param configSearchInVO
     * @param pageModel
     * @return
     */
    public PageResult<ConfigListOutVO> pageListByEntity(ConfigSearchInVO configSearchInVO, PageModel pageModel) {
        return toPageResult(configDao.pageListByEntity(configConvert.toConfig(configSearchInVO), pageModel).convert(configConvert::toOut));
    }

    /**
     * 根据配置key查询配置value
     *
     * @param key
     * @return
     */
    public String getValueByKey(String key) {
        return configDao.getValueByKey(key);
    }

    /**
     * 保存
     *
     * @param configCreateInVO
     * @return
     */
    public Boolean save(ConfigCreateInVO configCreateInVO) {
        return configDao.save(configConvert.toConfig(configCreateInVO));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    public Boolean delete(String id) {
        return configDao.removeById(id);
    }

    /**
     * 修改
     *
     * @param configUpdateInVO
     * @return
     */
    public Boolean update(ConfigUpdateInVO configUpdateInVO) {
        return configDao.updateById(configConvert.toConfig(configUpdateInVO));
    }

    /**
     * 批量删除
     *
     * @param idList
     * @return
     */
    public Boolean deleteBatch(List<String> idList) {
        return configDao.removeBatchByIds(idList);
    }
}
