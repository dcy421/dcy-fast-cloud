package com.dcy.system.biz.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import com.dcy.common.annotation.Log;
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import com.dcy.system.biz.constant.PermissionConstant;
import com.dcy.system.biz.service.ConfigService;
import com.dcy.system.biz.vo.in.ConfigCreateInVO;
import com.dcy.system.biz.vo.in.ConfigSearchInVO;
import com.dcy.system.biz.vo.in.ConfigUpdateInVO;
import com.dcy.system.biz.vo.out.ConfigListOutVO;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * <p>
 * 参数配置表 前端控制器
 * </p>
 *
 * @author dcy
 * @since 2019-09-06
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/config")
@ApiSupport(order = 30)
@Api(value = "ConfigController", tags = {"参数配置接口"})
public class ConfigController extends DcyBaseController {

    private final ConfigService configService;

    @Log
    @SaCheckPermission(value = PermissionConstant.CONFIG_LIST, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "配置分页查询")
    @GetMapping("/page")
    public R<PageResult<ConfigListOutVO>> pageList(ConfigSearchInVO configSearchInVO, PageModel pageModel) {
        return success(configService.pageListByEntity(configSearchInVO, pageModel));
    }

    @ApiOperation(value = "根据配置key查询配置value")
    @ApiImplicitParam(name = "key", value = "参数键名", dataType = "String", paramType = "query", required = true)
    @GetMapping(value = "/getValueByKey")
    public R<String> getValueByKey(@NotBlank(message = "参数键名不能为空") String key) {
        return success(configService.getValueByKey(key));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.CONFIG_SAVE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody ConfigCreateInVO configCreateInVO) {
        return success(configService.save(configCreateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.CONFIG_UPDATE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "修改")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody ConfigUpdateInVO configUpdateInVO) {
        return success(configService.update(configUpdateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.CONFIG_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "配置id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "配置id不能为空") @RequestParam String id) {
        return success(configService.delete(id));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.CONFIG_BATCH_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "根据list删除")
    @PostMapping(value = "/deleteBatch")
    public R<Boolean> deleteBatch(@NotEmpty(message = "集合不能为空") @ApiParam @RequestBody List<String> idList) {
        return success(configService.deleteBatch(idList));
    }

}
