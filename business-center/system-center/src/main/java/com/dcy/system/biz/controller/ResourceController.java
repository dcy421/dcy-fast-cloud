package com.dcy.system.biz.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaMode;
import com.dcy.common.annotation.Log;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import com.dcy.system.biz.constant.PermissionConstant;
import com.dcy.system.biz.service.ResourceService;
import com.dcy.system.biz.vo.in.ResourceCreateInVO;
import com.dcy.system.biz.vo.in.ResourceUpdateInVO;
import com.dcy.system.biz.vo.out.ResourceListOutVO;
import com.dcy.system.biz.vo.out.RouterListOutVO;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/8/26 9:31
 */
@RequiredArgsConstructor
@Validated
@RestController
@RequestMapping("/system/resource")
@ApiSupport(order = 10)
@Api(value = "ResourceController", tags = {"资源接口"})
public class ResourceController extends DcyBaseController {

    private final ResourceService resourceService;

    @Log
    @SaCheckPermission(value = {PermissionConstant.RESOURCE_LIST, PermissionConstant.USER_AUTH_ROLE}, mode = SaMode.OR, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "获取资源tree列表数据")
    @GetMapping(value = "/getResourceTreeList")
    public R<List<ResourceListOutVO>> getResourceTreeList() {
        return success(resourceService.getResourceTreeList());
    }

    @Log
    @ApiOperation(value = "获取菜单列表数据")
    @GetMapping(value = "/getMenuList")
    public R<List<ResourceListOutVO>> getMenuList() {
        return success(resourceService.getMenuList());
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.USER_AUTH_ROLE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "根据角色id获取资源tree列表数据")
    @ApiImplicitParam(name = "roleId", value = "角色id", dataType = "roleId", paramType = "query", required = true)
    @GetMapping(value = "/getResourceIdListByRoleId")
    public R<List<String>> getResourceIdListByRoleId(@NotBlank(message = "角色id不能为空") @RequestParam String roleId) {
        return success(resourceService.getResourceIdListByRoleId(roleId));
    }

    @Log
    @ApiOperation(value = "根据登录人获取目录和菜单路由（vue使用）")
    @GetMapping(value = "/getRouterList")
    public R<List<RouterListOutVO>> getResourceList() {
        return success(resourceService.getResourceList());
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.RESOURCE_ADD, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "添加")
    @PostMapping("/save")
    public R<Boolean> save(@Validated @ApiParam @RequestBody ResourceCreateInVO resourceCreateInVO) {
        return success(resourceService.save(resourceCreateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.RESOURCE_UPDATE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "修改")
    @PostMapping(value = "/update")
    public R<Boolean> update(@Validated @ApiParam @RequestBody ResourceUpdateInVO resourceUpdateInVO) {
        return success(resourceService.update(resourceUpdateInVO));
    }

    @Log
    @SaCheckPermission(value = PermissionConstant.RESOURCE_DELETE, type = StpAdminUtil.TYPE)
    @ApiOperation(value = "删除")
    @ApiImplicitParam(name = "id", value = "id", dataType = "String", paramType = "query", required = true)
    @PostMapping(value = "/delete")
    public R<Boolean> delete(@NotBlank(message = "id不能为空") @RequestParam String id) {
        return success(resourceService.delete(id));
    }
}
