package com.dcy.system.biz.dao;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dcy.common.model.PageModel;
import com.dcy.db.base.dao.DcyBaseDao;
import com.dcy.system.biz.model.Config;
import com.dcy.system.biz.mapper.ConfigMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2022-11-08
 */
@Service
public class ConfigDao extends DcyBaseDao<ConfigMapper, Config> {

    /**
     * 获取表格数据
     *
     * @param config
     * @param pageModel
     * @return
     */
    public IPage<Config> pageListByEntity(Config config, PageModel pageModel) {
        LambdaQueryWrapper<Config> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(config.getConfigName()), Config::getConfigName, config.getConfigName());
        return pageList(pageModel, queryWrapper);
    }

    /**
     * 根据配置key查询配置value
     *
     * @param key
     * @return
     */
    public String getValueByKey(String key) {
        String value = null;
        Config sysConfig = getOne(Wrappers.<Config>lambdaQuery().eq(Config::getConfigKey, key));
        if (sysConfig != null) {
            value = sysConfig.getConfigValue();
        }
        return value;
    }

}
