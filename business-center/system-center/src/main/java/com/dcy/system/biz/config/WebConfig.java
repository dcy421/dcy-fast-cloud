package com.dcy.system.biz.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.router.SaRouter;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @Author：dcy
 * @since 2021/3/29
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${ignored}")
    private List<String> ignored;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SaInterceptor(handler -> {
            // 拦截所有URL请求
            SaRouter.match("/**", r -> StpAdminUtil.checkLogin());
        })).addPathPatterns("/**").excludePathPatterns(ignored);
    }

}
