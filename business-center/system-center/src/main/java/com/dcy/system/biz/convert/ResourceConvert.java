package com.dcy.system.biz.convert;

import com.dcy.system.biz.model.Resource;
import com.dcy.system.biz.vo.in.ResourceCreateInVO;
import com.dcy.system.biz.vo.in.ResourceUpdateInVO;
import com.dcy.system.biz.vo.out.ResourceListOutVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 15:03
 */
@Mapper
public interface ResourceConvert {

    ResourceConvert INSTANCE = Mappers.getMapper(ResourceConvert.class);

    Resource toResource(ResourceCreateInVO resourceCreateInVO);

    Resource toResource(ResourceUpdateInVO resourceUpdateInVO);

    ResourceListOutVO toOut(Resource resource);

    List<ResourceListOutVO> toOutList(List<Resource> resourceList);
}
