package com.dcy.system.biz.enums;

import com.dcy.common.api.IErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author dcy
 */

@AllArgsConstructor
@Getter
public enum SystemApiErrorCode implements IErrorCode {
    // token错误
    USER_UPDATE_PASS_ERROR(1300001, "当前密码输入错误，请重新输入"),
    USER_UPDATE_PASS2_ERROR(1300002, "两次密码输入不一致，请重新输入"),
    ;

    private final Integer code;
    private final String msg;

}
