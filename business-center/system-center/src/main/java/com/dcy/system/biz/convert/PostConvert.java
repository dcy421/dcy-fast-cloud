package com.dcy.system.biz.convert;

import com.dcy.system.biz.model.Post;
import com.dcy.system.biz.vo.in.PostCreateInVO;
import com.dcy.system.biz.vo.in.PostSearchInVO;
import com.dcy.system.biz.vo.in.PostUpdateInVO;
import com.dcy.system.biz.vo.out.PostListOutVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 9:28
 */
@Mapper
public interface PostConvert {

    PostConvert INSTANCE = Mappers.getMapper(PostConvert.class);

    Post toPost(PostSearchInVO postSearchInVO);

    Post toPost(PostCreateInVO postCreateInVO);

    Post toPost(PostUpdateInVO postUpdateInVO);

    PostListOutVO toOut(Post post);

    List<PostListOutVO> toOutList(List<Post> posts);


}
