package com.dcy.system.biz.convert;

import com.dcy.system.biz.model.DictType;
import com.dcy.system.biz.vo.in.DictTypeCreateInVO;
import com.dcy.system.biz.vo.in.DictTypeSearchInVO;
import com.dcy.system.biz.vo.in.DictTypeUpdateInVO;
import com.dcy.system.biz.vo.out.DictTypeListOutVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 9:28
 */
@Mapper
public interface DictTypeConvert {

    DictTypeConvert INSTANCE = Mappers.getMapper(DictTypeConvert.class);

    DictType toDictType(DictTypeSearchInVO dictTypeSearchInVO);

    DictType toDictType(DictTypeCreateInVO dictTypeCreateInVO);

    DictType toDictType(DictTypeUpdateInVO dictTypeUpdateInVO);

    DictTypeListOutVO toOut(DictType dictType);

}
