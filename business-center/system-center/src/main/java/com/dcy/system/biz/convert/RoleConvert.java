package com.dcy.system.biz.convert;

import com.dcy.system.api.dto.out.RoleListOutDTO;
import com.dcy.system.biz.model.Role;
import com.dcy.system.biz.vo.in.RoleCreateInVO;
import com.dcy.system.biz.vo.in.RoleSearchInVO;
import com.dcy.system.biz.vo.in.RoleUpdateInVO;
import com.dcy.system.biz.vo.out.RoleListOutVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/12/17 8:47
 */
@Mapper
public interface RoleConvert {

    RoleConvert INSTANCE = Mappers.getMapper(RoleConvert.class);

    Role toRole(RoleSearchInVO roleSearchInVO);

    Role toRole(RoleCreateInVO roleCreateInVO);

    Role toRole(RoleUpdateInVO roleUpdateInVO);

    RoleListOutVO toOut(Role role);
    RoleListOutDTO toOutDto(Role role);

    List<RoleListOutVO> toOutList(List<Role> roles);

    List<RoleListOutDTO> toOutDtoList(List<Role> roles);


}

