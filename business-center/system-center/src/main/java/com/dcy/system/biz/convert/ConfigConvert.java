package com.dcy.system.biz.convert;

import com.dcy.system.biz.model.Config;
import com.dcy.system.biz.vo.in.ConfigCreateInVO;
import com.dcy.system.biz.vo.in.ConfigSearchInVO;
import com.dcy.system.biz.vo.in.ConfigUpdateInVO;
import com.dcy.system.biz.vo.out.ConfigListOutVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 15:03
 */
@Mapper
public interface ConfigConvert {

    ConfigConvert INSTANCE = Mappers.getMapper(ConfigConvert.class);

    Config toConfig(ConfigCreateInVO configCreateInVO);

    Config toConfig(ConfigUpdateInVO configUpdateInVO);

    Config toConfig(ConfigSearchInVO configSearchInVO);

    ConfigListOutVO toOut(Config config);

}
