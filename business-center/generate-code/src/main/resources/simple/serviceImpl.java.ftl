package ${package.ServiceImpl};

import ${superServiceImplClassPackage};
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import ${dtoMapperPackage}.${entity}Convert;
import ${daoPackage}.${entity}Dao;
import ${inputDtoPackage}.${entity}CreateInVO;
import ${inputDtoPackage}.${entity}SearchInVO;
import ${inputDtoPackage}.${entity}UpdateInVO;
import ${outputDtoPackage}.${entity}ListOutVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@RequiredArgsConstructor
@Service
public class ${table.serviceImplName} extends ${superServiceImplClass} {

    private final ${entity}Dao ${entity?uncap_first}Dao;
    private final ${entity}Convert ${entity?uncap_first}Convert = ${entity}Convert.INSTANCE;

    /**
     * 获取表格数据
     *
     * @param ${entity?uncap_first}
     * @param pageModel
     * @return
     */
    public PageResult<${entity}ListOutputDTO> pageListByEntity(${entity}SearchInVO ${entity?uncap_first}SearchInVO, PageModel pageModel) {
        return toPageResult(${entity?uncap_first}Dao.pageListByEntity(${entity?uncap_first}Convert.to${entity}(${entity?uncap_first}SearchInVO), pageModel).convert(${entity?uncap_first}Convert::toOut));
    }

    /**
     * 保存
     *
     * @param ${entity?uncap_first}CreateInVO
     * @return
     */
    public Boolean save(${entity}CreateInVO ${entity?uncap_first}CreateInVO) {
        return ${entity?uncap_first}Dao.save(${entity?uncap_first}Convert.to${entity}(${entity?uncap_first}CreateInVO));
    }

    /**
     * 修改
     *
     * @param ${entity?uncap_first}UpdateInVO
     * @return
     */
    public Boolean update(${entity}UpdateInVO ${entity?uncap_first}UpdateInVO) {
        return ${entity?uncap_first}Dao.updateById(${entity?uncap_first}Convert.to${entity}(${entity?uncap_first}UpdateInVO));
    }

    /**
     * 删除
     *
     * @param id
     * @return
     */
    public Boolean delete(String id) {
        return ${entity?uncap_first}Dao.removeById(id);
    }

    /**
     * 批量删除
     *
     * @param idList
     * @return
     */
    public Boolean deleteBatch(List<String> idList) {
        return ${entity?uncap_first}Dao.removeBatchByIds(idList);
    }


}
