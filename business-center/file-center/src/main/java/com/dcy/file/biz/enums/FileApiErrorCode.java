package com.dcy.file.biz.enums;

import com.dcy.common.api.IErrorCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 接口返回枚举
 *
 * @author dcy
 */
@AllArgsConstructor
@Getter
public enum FileApiErrorCode implements IErrorCode {
    FILE_UPLOAD_FORMAT_ERROR(1400001, "文件上传格式错误"),
    FILE_UPLOAD_ERROR(1400002, "文件上传错误"),
    FILE_DOWNLOAD_ERROR(1400003, "文件下载错误"),
    FILE_REMOVE_ERROR(1400004, "文件删除错误"),
    ;

    private final Integer code;
    private final String msg;

}
