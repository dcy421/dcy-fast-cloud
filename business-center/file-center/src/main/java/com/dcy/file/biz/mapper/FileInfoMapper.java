package com.dcy.file.biz.mapper;

import com.dcy.db.base.mapper.DcyBaseMapper;
import com.dcy.file.biz.model.FileInfo;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2019-09-18
 */
public interface FileInfoMapper extends DcyBaseMapper<FileInfo> {

}
