package com.dcy.file.biz.vo.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author：dcy
 * @Description: 下载文件dto
 * @Date: 2020/10/22 13:52
 */
@Data
@ApiModel
public class DownloadInVO {

    @ApiModelProperty(value = "文件名称")
    @NotBlank(message = "文件名称不能为空")
    private String fileName;

    @ApiModelProperty(value = "文件路径")
    @NotBlank(message = "文件路径不能为空")
    private String filePath;
}
