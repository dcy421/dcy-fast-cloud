package com.dcy.file.biz.util;

import cn.hutool.core.io.FileUtil;
import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * @author dcy
 * @description 文件工具扩展
 * @createTime 2023/1/21 22:09
 */
public class FileExtNameUtil {
    private static final List<String> SUPPORT_FILE_EXT_FORMATS = ImmutableList.of(
            // office
            "doc", "docx", "xls", "xlsx", "ppt", "pptx", "pdf",
            // 图片
            "jpg", "jpeg", "png", "gif", "bmp",
            // 音频
            "mp3", "wma", "flac", "wav", "amr",
            // 视频
            "wmv", "mp4", "mov", "3gp",
            // 文本
            "txt"
    );

    /**
     * 根据文件名查询是否可以上传
     *
     * @param fileName
     * @return
     */
    public static boolean checkFileExtName(String fileName) {
        final String extName = FileUtil.extName(fileName);
        return SUPPORT_FILE_EXT_FORMATS.stream().anyMatch(extName::equalsIgnoreCase);
    }

}
