package com.dcy.file.biz.dao;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.dcy.common.model.PageModel;
import com.dcy.db.base.dao.DcyBaseDao;
import com.dcy.file.biz.mapper.FileInfoMapper;
import com.dcy.file.biz.model.FileInfo;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 参数配置表 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2022-11-08
 */
@Service
public class FileInfoDao extends DcyBaseDao<FileInfoMapper, FileInfo> {

    /**
     * 分页查询
     *
     * @param fileInfo
     * @return
     */
    public IPage<FileInfo> pageListByEntity(FileInfo fileInfo, PageModel pageModel) {
        LambdaQueryWrapper<FileInfo> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.like(StrUtil.isNotBlank(fileInfo.getName()), FileInfo::getName, fileInfo.getName());
        return pageList(pageModel, queryWrapper);
    }
}
