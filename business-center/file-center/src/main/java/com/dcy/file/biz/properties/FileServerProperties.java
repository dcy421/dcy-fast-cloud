package com.dcy.file.biz.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2019/9/18 13:33
 */
@Setter
@Getter
@ConfigurationProperties(prefix = "dcy.file-server")
@Component
@RefreshScope
public class FileServerProperties {
    /**
     * 为以下4个值，指定不同的自动化配置
     * qiniu：七牛oss
     * aliyun：阿里云oss
     * minio：本地部署的minio
     */
    private String type;

    /**
     * oss配置
     */
    private OssProperties oss = new OssProperties();

    /**
     * minio配置
     */
    private MinioProperties minio = new MinioProperties();
}
