package com.dcy.file.biz.config.file;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.http.HttpUtil;
import com.dcy.common.exception.BusinessException;
import com.dcy.file.biz.enums.FileApiErrorCode;
import com.dcy.file.biz.model.FileInfo;
import com.dcy.file.biz.properties.FileServerProperties;
import com.dcy.file.biz.service.IFileHandler;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * @Author：dcy
 * @Description: 七牛云配置
 * @Date: 2019/9/18 13:59
 */
@Configuration
@ConditionalOnProperty(name = "dcy.file-server.type", havingValue = "qiniu")
public class QiniuOSSAutoConfigure {

    @Autowired
    private FileServerProperties fileProperties;


    /**
     * 华东机房
     */
    @Bean
    public com.qiniu.storage.Configuration qiniuConfig() {
        return new com.qiniu.storage.Configuration(Zone.autoZone());
    }

    /**
     * 构建一个七牛上传工具实例
     */
    @Bean
    public UploadManager uploadManager() {
        return new UploadManager(qiniuConfig());
    }

    /**
     * 认证信息实例
     *
     * @return
     */
    @Bean
    public Auth auth() {
        return Auth.create(fileProperties.getOss().getAccessKey(), fileProperties.getOss().getAccessKeySecret());
    }

    /**
     * 构建七牛空间管理实例
     */
    @Bean
    public BucketManager bucketManager() {
        return new BucketManager(auth(), qiniuConfig());
    }


    @RequiredArgsConstructor
    @Slf4j
    @Service
    public class QiniuOssServiceImpl implements IFileHandler {

        private final UploadManager uploadManager;
        private final BucketManager bucketManager;
        private final Auth auth;

        @Override
        public String fileType() {
            return fileProperties.getType();
        }

        @Override
        public void uploadFile(MultipartFile file, FileInfo fileInfo) {
            try {
                // 调用put方法上传
                uploadManager.put(file.getInputStream(), fileInfo.getName(), auth.uploadToken(fileProperties.getOss().getBucketName()), null, null);
                fileInfo.setUrl(fileProperties.getOss().getEndpoint() + "/" + fileInfo.getName());
            } catch (Exception e) {
                log.error("uploadFile {}", e.getMessage());
                throw new BusinessException(FileApiErrorCode.FILE_UPLOAD_ERROR);
            }
        }

        @Override
        public void deleteFile(FileInfo fileInfo) {
            try {
                Response response = bucketManager.delete(fileProperties.getOss().getBucketName(), fileInfo.getPath());
                int retry = 0;
                while (response.needRetry() && retry++ < 3) {
                    response = bucketManager.delete(fileProperties.getOss().getBucketName(), fileInfo.getPath());
                }
            } catch (QiniuException e) {
                log.error("deleteFile {}", e.getMessage());
                throw new BusinessException(FileApiErrorCode.FILE_REMOVE_ERROR);
            }
        }

        @Override
        public void downLoadFile(String fileName, String filePath, HttpServletResponse response) {
            // 获取下载文件路径
            String targetUrl = fileProperties.getOss().getEndpoint() + "/" + fileName;
            String url = auth.privateDownloadUrl(targetUrl);
            try {
                response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode(fileName, CharsetUtil.UTF_8));
                response.setCharacterEncoding(CharsetUtil.UTF_8);
                // 执行下载文件s
                HttpUtil.download(url, response.getOutputStream(), true);
            } catch (IOException e) {
                log.error("downloadFile {}", e.getMessage());
                throw new BusinessException(FileApiErrorCode.FILE_DOWNLOAD_ERROR);
            }
        }
    }
}
