package com.dcy.file.biz.service;

import com.dcy.file.biz.model.FileInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * @Author：congyt
 * @Description: 文件操作接口
 * @Date: 2019/9/18 13:51
 */
public interface IFileHandler {
    /**
     * 文件来源
     *
     * @return
     */
    String fileType();

    /**
     * 上传文件
     *
     * @param file
     * @param fileInfo
     */
    void uploadFile(MultipartFile file, FileInfo fileInfo);


    /**
     * 删除文件资源
     *
     * @param fileInfo
     * @return
     */
    void deleteFile(FileInfo fileInfo);

    /**
     * 下载文件
     *
     * @param fileName
     * @param filePath
     * @param response
     * @return
     */
    void downLoadFile(String fileName, String filePath, HttpServletResponse response);

}
