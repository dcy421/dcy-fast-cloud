package com.dcy.file.biz.service;

import cn.hutool.core.util.StrUtil;
import com.dcy.common.exception.BusinessException;
import com.dcy.common.model.PageModel;
import com.dcy.common.model.PageResult;
import com.dcy.db.base.service.DcyBaseService;
import com.dcy.file.biz.convert.FileInfoConvert;
import com.dcy.file.biz.dao.FileInfoDao;
import com.dcy.file.biz.enums.FileApiErrorCode;
import com.dcy.file.biz.model.FileInfo;
import com.dcy.file.biz.util.FileExtNameUtil;
import com.dcy.file.biz.vo.in.FileInfoSearchInVO;
import com.dcy.file.biz.vo.out.FileInfoListOutVO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2019/12/17 9:03
 */
@RequiredArgsConstructor
@Service
public class FileInfoService extends DcyBaseService {

    private final IFileHandler iFileHandler;
    private final FileInfoDao fileInfoDao;
    private final FileInfoConvert fileInfoConvert = FileInfoConvert.INSTANCE;

    /**
     * 获取表格数据
     *
     * @param fileInfoSearchInVO
     * @param pageModel
     * @return
     */
    public PageResult<FileInfoListOutVO> pageListByEntity(FileInfoSearchInVO fileInfoSearchInVO, PageModel pageModel) {
        return toPageResult(fileInfoDao.pageListByEntity(fileInfoConvert.toFileInfo(fileInfoSearchInVO), pageModel).convert(fileInfoConvert::toOut));
    }

    /**
     * 文件上传
     *
     * @param file
     * @return
     */
    public FileInfoListOutVO upload(MultipartFile file) {
        if (!FileExtNameUtil.checkFileExtName(file.getOriginalFilename())) {
            throw new BusinessException(FileApiErrorCode.FILE_UPLOAD_FORMAT_ERROR);
        }
        return fileInfoConvert.toOut(uploadCommonFile(file));
    }

    private FileInfo uploadCommonFile(MultipartFile file) {
        FileInfo fileInfo = new FileInfo()
                .setName(file.getOriginalFilename())
                .setContentType(file.getContentType())
                .setFileSize(file.getSize())
                .setCreateDate(new Date());
        iFileHandler.uploadFile(file, fileInfo);
        // 设置文件来源
        fileInfo.setSource(iFileHandler.fileType());
        // 将文件信息保存到数据库
        fileInfoDao.save(fileInfo);
        return fileInfo;
    }

    /**
     * 多文件上传
     *
     * @param files
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public List<FileInfoListOutVO> uploadFiles(MultipartFile[] files) {
        if (Arrays.stream(files).anyMatch(multipartFile -> !FileExtNameUtil.checkFileExtName(multipartFile.getOriginalFilename()))) {
            throw new BusinessException(FileApiErrorCode.FILE_UPLOAD_FORMAT_ERROR);
        }
        return fileInfoConvert.toOutList(Arrays.stream(files).map(this::uploadCommonFile).collect(Collectors.toList()));
    }

    /**
     * 删除文件
     *
     * @param id
     */
    public void deleteFile(String id) {
        FileInfo fileInfo = fileInfoDao.getById(id);
        if (fileInfo != null) {
            fileInfoDao.removeById(fileInfo.getId());
            iFileHandler.deleteFile(fileInfo);
        }
    }

    /**
     * 下载文件
     *
     * @param fileName
     * @param filePath
     * @param response
     */
    public void downLoad(String fileName, String filePath, HttpServletResponse response) {
        if (StrUtil.isNotBlank(fileName) && StrUtil.isNotBlank(filePath)) {
            iFileHandler.downLoadFile(fileName, filePath, response);
        }
    }

}
