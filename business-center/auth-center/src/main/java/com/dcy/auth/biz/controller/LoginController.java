package com.dcy.auth.biz.controller;

import cn.dev33.satoken.annotation.SaCheckLogin;
import com.dcy.auth.biz.service.LoginService;
import com.dcy.auth.biz.vo.in.LoginInVO;
import com.dcy.common.annotation.Log;
import com.dcy.common.enums.LogEnum;
import com.dcy.common.model.R;
import com.dcy.satoken.model.LoginAccount;
import com.dcy.satoken.util.satoken.StpAdminUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dcy
 * @Date: 2019/3/22 10:17
 * @Description:
 */
@RequiredArgsConstructor
@Validated
@RestController
@Api(tags = {"登录接口"})
public class LoginController {

    private final LoginService loginService;

    @Log(type = LogEnum.LOGIN)
    @ApiOperation(value = "用户登录")
    @ApiOperationSupport(order = 1, author = "dcy")
    @PostMapping("/login")
    public R<String> login(@Validated @ApiParam @RequestBody LoginInVO loginInVO) {
        return R.success(loginService.login(loginInVO));
    }

    @Log(type = LogEnum.BUSINESS)
    @SaCheckLogin(type = StpAdminUtil.TYPE)
    @ApiOperation(value = "获取用户信息")
    @ApiOperationSupport(order = 5, author = "dcy")
    @GetMapping("/getUserInfo")
    public R<LoginAccount> getUserInfo() {
        return R.success(loginService.getUserInfo());
    }

    @Log(type = LogEnum.LOGOUT)
    @SaCheckLogin(type = StpAdminUtil.TYPE)
    @ApiOperation(value = "用户退出")
    @ApiOperationSupport(order = 10, author = "dcy")
    @PostMapping("/logout")
    public R<Boolean> logout() {
        return R.success(loginService.logout());
    }
}
