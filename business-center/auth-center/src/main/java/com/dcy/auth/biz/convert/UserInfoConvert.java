package com.dcy.auth.biz.convert;

import com.dcy.satoken.model.LoginAccount;
import com.dcy.system.api.dto.out.UserInfoListOutDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2020/10/23 9:28
 */
@Mapper
public interface UserInfoConvert {

    UserInfoConvert INSTANCE = Mappers.getMapper(UserInfoConvert.class);

    LoginAccount toLoginAccount(UserInfoListOutDTO userInfoListOutDTO);
}
