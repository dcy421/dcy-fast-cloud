package com.dcy.auth.biz.config;

import cn.dev33.satoken.interceptor.SaInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @author Tango
 * @since 2021/3/29
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Value("${ignored}")
    private List<String> ignored;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // sa-token 拦截器
        registry.addInterceptor(new SaInterceptor()).addPathPatterns("/**").excludePathPatterns(ignored);
    }

}
