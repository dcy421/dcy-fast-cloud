package com.dcy.seataaccount.biz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dcy
 */
@SpringBootApplication(scanBasePackages = "com.dcy")
@MapperScan(basePackages = {"com.dcy.*.biz.mapper"})
public class SeataAccountCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeataAccountCenterApplication.class, args);
    }

}
