package com.dcy.seataaccount.biz.controller;

import com.dcy.common.annotation.Log;
import com.dcy.common.model.R;
import com.dcy.db.base.controller.DcyBaseController;
import com.dcy.seataaccount.biz.service.AccountTblService;
import com.dcy.seataorder.api.model.OrderTbl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author dcy
 * @since 2021-04-23
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/account-tbl")
@Api(value = "AccountTblController", tags = {"操作接口"})
public class AccountTblController extends DcyBaseController {

    private final AccountTblService accountTblService;

    @Log
    @ApiOperation(value = "采购")
    @PostMapping(value = "/purchase")
    public R<OrderTbl> purchase(String userId, String commodityCode, int orderCount, int ex) {
        // C201901140001
        return success(accountTblService.purchase(userId, commodityCode, orderCount, ex));
    }
}
