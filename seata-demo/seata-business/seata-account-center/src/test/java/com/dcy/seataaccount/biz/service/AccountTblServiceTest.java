package com.dcy.seataaccount.biz.service;

import com.dcy.seataaccount.biz.SeataAccountCenterApplication;
import com.dcy.seataorder.api.model.OrderTbl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = {SeataAccountCenterApplication.class})
@Slf4j
class AccountTblServiceTest {

    @Autowired
    private AccountTblService accountTblService;

    @Test
    void purchaseTest(){
        OrderTbl orderTbl = accountTblService.purchase("1", "C201901140001", 1,0);
        log.info(" orderTbl -=- {}",orderTbl);
    }
}