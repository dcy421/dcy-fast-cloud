package com.dcy.seatastorage.biz.mapper;

import com.dcy.seatastorage.api.model.StorageTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2021-04-23
 */
public interface StorageTblMapper extends BaseMapper<StorageTbl> {

}
