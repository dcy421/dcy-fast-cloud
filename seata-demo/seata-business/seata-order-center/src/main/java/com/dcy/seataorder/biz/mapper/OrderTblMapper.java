package com.dcy.seataorder.biz.mapper;

import com.dcy.seataorder.api.model.OrderTbl;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author dcy
 * @since 2021-04-23
 */
public interface OrderTblMapper extends BaseMapper<OrderTbl> {

}
