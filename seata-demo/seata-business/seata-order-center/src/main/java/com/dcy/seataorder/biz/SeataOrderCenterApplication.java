package com.dcy.seataorder.biz;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author dcy
 */
@SpringBootApplication(scanBasePackages = "com.dcy")
@MapperScan(basePackages = {"com.dcy.*.biz.mapper"})
public class SeataOrderCenterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeataOrderCenterApplication.class, args);
    }

}
