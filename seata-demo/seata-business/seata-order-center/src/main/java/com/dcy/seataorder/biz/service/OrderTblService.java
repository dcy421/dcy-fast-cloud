package com.dcy.seataorder.biz.service;

import com.dcy.db.base.service.DcyBaseService;
import com.dcy.seataaccount.api.api.AAccountService;
import com.dcy.seataorder.api.api.AOrderService;
import com.dcy.seataorder.api.model.OrderTbl;
import com.dcy.seataorder.biz.mapper.OrderTblMapper;
import io.seata.core.context.RootContext;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author dcy
 * @since 2021-04-23
 */
@RequiredArgsConstructor
@Slf4j
@DubboService(interfaceClass = AOrderService.class)
public class OrderTblService extends DcyBaseService implements AOrderService {

    private final OrderTblMapper orderTblMapper;
    @DubboReference
    private AAccountService aAccountService;

    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    @Override
    public OrderTbl create(String userId, String commodityCode, int orderCount) {
        log.info("当前 XID: {}", RootContext.getXID());
        // 扣钱
        OrderTbl orderTblRes = null;
        boolean debit = aAccountService.debit(userId, 100);
        if (debit) {
            OrderTbl orderTbl = new OrderTbl();
            orderTbl.setUserId(userId);
            orderTbl.setCommodityCode(commodityCode);
            orderTbl.setCount(orderCount);
            orderTbl.setMoney(100d);
            // 添加订单
            if (orderTblMapper.insert(orderTbl) > 0) {
                orderTblRes = orderTbl;
            }
        }
        return orderTblRes;
    }

}
