package com.dcy.seatastorage.api.api;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/4/23 9:53
 */
public interface AStorageService {

    /**
     * 扣除存储数量
     * @param commodityCode 商品代码
     * @param count         扣减数量
     * @return
     */
    boolean deduct(String commodityCode, int count);

}
