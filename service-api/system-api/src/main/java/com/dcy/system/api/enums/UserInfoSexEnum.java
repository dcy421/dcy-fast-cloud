package com.dcy.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * @Author：dcy
 * @Description: 用户状态枚举类
 * @Date: 2021/8/24 11:08
 */
@AllArgsConstructor
@Getter
public enum UserInfoSexEnum {

    //性别（0、男；1、女）
    MAN("0", "男"),
    WOMAN("1", "女"),
    ;

    private final String code;
    private final String name;

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static UserInfoSexEnum getByCode(String code) {
        return Stream.of(UserInfoSexEnum.values())
                .filter(userInfoStatusEnum -> userInfoStatusEnum.getCode().equals(code))
                .findAny()
                .orElse(null);
    }

    /**
     * 根据code返回名称
     *
     * @param code
     * @return
     */
    public static String getNameByCode(String code) {
        return Optional.ofNullable(getByCode(code)).map(UserInfoSexEnum::getName).orElse(null);
    }
}
