package com.dcy.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/8/24 11:04
 */
@AllArgsConstructor
@Getter
public enum ResourcesStatusEnum {

    //资源 状态（0、正常；1、禁用）
    NORMAL("0", "正常"),
    DISABLE("1", "禁用"),
    ;

    private final String code;
    private final String name;

    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static ResourcesStatusEnum getByCode(String code) {
        return Stream.of(ResourcesStatusEnum.values())
                .filter(resourcesStatusEnum -> resourcesStatusEnum.code.equals(code))
                .findAny()
                .orElse(null);
    }
    
}
