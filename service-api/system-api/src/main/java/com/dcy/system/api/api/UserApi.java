package com.dcy.system.api.api;

import com.dcy.system.api.dto.out.UserInfoListOutDTO;

/**
 * @Author：dcy
 * @Description: 用户服务调用
 * @Date: 2021/4/15 8:42
 */
public interface UserApi {

    /**
     * 根据用户名称查询对象
     *
     * @param username
     * @return
     */
    UserInfoListOutDTO getUserInfoByUsername(String username);

}
