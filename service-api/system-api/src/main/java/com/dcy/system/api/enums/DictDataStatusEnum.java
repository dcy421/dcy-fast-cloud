package com.dcy.system.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * @Author：dcy
 * @Description:
 * @Date: 2021/8/24 11:10
 */
@AllArgsConstructor
@Getter
public enum DictDataStatusEnum {

    //字典数据 状态（0正常 1停用）
    NORMAL("0", "正常"),
    DEACTIVATE("1", "停用"),
    ;

    private final String code;
    private final String name;


    /**
     * 根据code取枚举对象
     *
     * @param code
     * @return
     */
    public static DictDataStatusEnum getByCode(String code) {
        return Stream.of(DictDataStatusEnum.values())
                .filter(dictDataStatusEnum -> dictDataStatusEnum.code.equals(code))
                .findAny()
                .orElse(null);
    }
}
