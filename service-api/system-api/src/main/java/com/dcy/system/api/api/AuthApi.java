package com.dcy.system.api.api;

import com.dcy.system.api.dto.out.RoleListOutDTO;

import java.util.List;
import java.util.Set;

/**
 * @author dcy
 * @description 角色服务调用
 * @createTime 2022/12/29 11:00
 */
public interface AuthApi {

    /**
     * 根据用户id查询权限集合和角色标识
     *
     * @param userId
     * @return
     */
    List<String> getAuthRoleAndResourceByUserId(String userId);

    /**
     * 根据用户id查询权限集合
     *
     * @param userId
     * @return
     */
    List<String> getPermissionListByUserId(String userId);

    /**
     * 根据用户id查询角色集合
     *
     * @param userId
     * @return
     */
    List<RoleListOutDTO> getAuthRoleListByUserId(String userId);


    /**
     * 获取是否拥有所有数据权限
     *
     * @param userId 用户id
     * @return true：拥有所有的数据权限；false：未拥有
     */
    Boolean getAllDataScopeFlag(String userId);


    /**
     * 根据角色id获取权限集合
     *
     * @param userId
     * @return
     */
    Set<String> getDataScopeListByUserId(String userId);
}
